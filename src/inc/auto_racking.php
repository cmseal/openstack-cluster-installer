<?php

function load_racking_info_config($con, $conf){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";

    $json_file = file_get_contents("/etc/openstack-cluster-installer/auto-racking.json");
    if($json_file === FALSE){
        $json["status"] = "error";
        $json["message"] = "Cannot load /etc/openstack-cluster-installer/auto-racking.json.";
        return $json;
    }
    $racking_info = json_decode($json_file, TRUE);
    if($racking_info === FALSE){
        $json["status"] = "error";
        $json["message"] = "Cannot decode /etc/openstack-cluster-installer/auto-racking.json.";
        return $json;
    }
    $json["data"] = $racking_info;
    return $json;
}

function auto_racking_guess_location($con, $conf, $machine_id){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";

    # Check if the machine exists
    $q = "SELECT * FROM machines WHERE id='$machine_id'";
    $r = mysqli_query($con, $q);
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n < 1){
        $json["status"] = "error";
        $json["message"] = "Cannot find a machine with id $machine_id.";
        return $json;
    }
    $machine = mysqli_fetch_array($r);

    $ret = load_racking_info_config($con, $conf);
    if($ret["status"] != "success"){
        return $ret;
    }
    $racking_info = $ret["data"];

    # Seach for its NICs (filtering those without switch hostname)
    $q = "SELECT * FROM ifnames WHERE machine_id='$machine_id' AND switch_hostname!='unknown' AND switchport_name!='unknown'";
    $r = mysqli_query($con, $q);
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n == 0){
        # No $json["data"] means nothing found.
        return $json;
    }

    # Search for a known switch hostname, and return it's info from the json file
    for($i=0;$i<$n;$i++){
        $nic = mysqli_fetch_array($r);
        if(isset($racking_info["switchhostnames"][ $nic["switch_hostname"] ])){
            # Return racking info
            $rackme = $racking_info["switchhostnames"][ $nic["switch_hostname"] ];
            # Search for number of U
            if(isset($racking_info["productnames"][ $machine["product_name"] ])){
                $num_of_u = $racking_info["productnames"][ $machine["product_name"] ]["num-u"];
                if(isset($racking_info["switchportnames"][ $nic["switchport_name"] ])){
                    $ustart = $racking_info["switchportnames"][ $nic["switchport_name"] ];
                    $uend = $ustart + $num_of_u - 1;
                    $rackme["u_start"] = $ustart;
                    $rackme["u_end"] = $uend;
                    $rackme["switch_hostname"] = $nic["switch_hostname"];
                    $json["data"] = $rackme;
                    return $json;
                }
            }
        }
    }
    # No $json["data"] means nothing found.
    return $json;
}

function auto_racking_auto_assign_location($con, $conf, $machine_id){
    $json["status"] = "success";
    $json["message"] = "Successfuly queried API.";

    # Check if the machine exists
    $q = "SELECT * FROM machines WHERE id='$machine_id'";
    $r = mysqli_query($con, $q);
    if($r === FALSE){
        $json["status"] = "error";
        $json["message"] = mysqli_error($con);
        return $json;
    }
    $n = mysqli_num_rows($r);
    if($n < 1){
        $json["status"] = "error";
        $json["message"] = "Cannot find a machine with id $machine_id.";
        return $json;
    }
    $machine = mysqli_fetch_array($r);

    $ret = auto_racking_guess_location($con, $conf, $machine["id"]);
    if($ret["status"] != "success"){
        return $ret;
    }
    if(isset($ret["data"])){
        $q = "UPDATE machines SET loc_dc='".$ret["data"]["dc"]."', loc_row='".$ret["data"]["row"]."', loc_rack='".$ret["data"]["rack"]."', loc_u_start='".$ret["data"]["u_start"]."', loc_u_end='".$ret["data"]["u_end"]."' WHERE id='".$machine["id"]."'";
        $r = mysqli_query($con, $q);
        if($r === FALSE){
            $json["status"] = "error";
            $json["message"] = mysqli_error($con);
            return $json;
        }
        $json["data"] = "done";
        return $json;
    }
    $json["data"] = "notfound";
    return $json;
}

?>