#!/bin/sh

### BEGIN INIT INFO
# Provides:          oci-agent-daemon
# Required-Start:    $network $local_fs
# Required-Stop:     $network $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: A small daemon to report config of slave nodes
# Description:       A small daemon which starts a shell script that
#                    reports the hardware configuration of slave nodes
#                    back to the central server of OCI.
### END INIT INFO

DESC="OpenStack cluster installer agent daemon (oci-agent-daemon)"
NAME=oci-agent-daemon
DAEMON=/usr/bin/${NAME}
PIDFILE=/var/run/oci-agent-daemon.pid
DAEMON=/usr/bin/oci-agent-daemon

. /lib/lsb/init-functions

do_start() {
	start-stop-daemon \
		--start \
		--quiet \
		--background \
		--make-pidfile --pidfile ${PIDFILE} \
		--startas $DAEMON \
		--test > /dev/null \
		|| return 1
	start-stop-daemon \
		--start \
		--quiet \
		--background \
		--make-pidfile --pidfile ${PIDFILE} \
		--startas $DAEMON \
		|| return 2
}

do_stop() {
	start-stop-daemon \
		--stop \
		--quiet \
		--retry=TERM/30/KILL/5 \
		--pidfile $PIDFILE
	RETVAL=$?
	rm -f $PIDFILE
	return "$RETVAL"
}

do_systemd_start() {
	exec $DAEMON
}

case "$1" in
start|systemd-start)
	log_daemon_msg "Starting $DESC" "$NAME"
	do_start
	case $? in
		0|1) log_end_msg 0 ; RET=$? ;;
		2)   log_end_msg 1 ; RET=$? ;;
		esac
;;
stop)
	log_daemon_msg "Stopping $DESC" "$NAME"
	do_stop
	case $? in
		0|1) log_end_msg 0 ; RET=$? ;;
		2)   log_end_msg 1 ; RET=$? ;;
        esac
;;
restart|reload|force-reload)
        $0 stop
        sleep 1
        $0 start
;;
*)
        echo 'Usage: $0 {start|stop|restart|reload}'
        exit 1
;;
esac

exit 0
