Source: openstack-cluster-installer
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 openstack-pkg-tools,
Standards-Version: 4.1.4
Homepage: https://salsa.debian.org/openstack-team/debian/openstack-cluster-installer
Vcs-Browser: https://salsa.debian.org/openstack-team/debian/openstack-cluster-installer
Vcs-Git: https://salsa.debian.org/openstack-team/debian/openstack-cluster-installer.git

Package: openstack-cluster-installer
Architecture: all
Depends:
 adduser,
 apache2,
 apache2-utils,
 bind9,
 bind9-host,
 bind9utils,
 bridge-utils,
 ceph-common,
 curl,
 dbconfig-common,
 default-mysql-server,
 ilorest,
 ipcalc,
 ipmitool,
 isc-dhcp-server,
 jq,
 live-build,
 lsb-base,
 net-tools,
 openstack-cluster-installer-cli (= ${binary:Version}),
 openstack-debian-images,
 openstack-pkg-tools,
 openstack-puppet-modules,
 php,
 php-cli,
 php-mysql,
 php-dapphp-radius | php-radius,
 php-ssh2,
 php-yaml,
 puppet-master,
 puppet-module-camptocamp-systemd,
 puppet-module-heini-wait-for,
 puppet-module-icann-quagga,
 puppet-module-oci (= ${binary:Version}),
 puppet-module-puppetlabs-firewall,
 pxelinux,
 qemu-utils,
 sudo,
 swift,
 tftpd-hpa,
 ${misc:Depends},
Description: automatic PXE and puppet-master installer for OpenStack
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package contains the provisionning server.

Package: openstack-cluster-installer-agent
Architecture: all
Depends:
 openstack-cluster-installer-common (= ${binary:Version}),
 ${misc:Depends},
Breaks:
 openstack-cluster-installer (<< 27~),
Replaces:
 openstack-cluster-installer (<< 27~),
Description: automatic PXE and puppet-master installer for OpenStack - agent
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package contains the necessary tooling inside the Debian Live environment
 to do hardware discovery, which will be reported back to the centralized OCI
 REST API.

Package: openstack-cluster-installer-cli
Architecture: all
Depends:
 csvkit,
 curl,
 gridsite-clients,
 q-text-as-data,
 ${misc:Depends},
Description: automatic PXE and puppet-master installer for OpenStack - API client
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package contains the command line interface (CLI) API client.

Package: openstack-cluster-installer-common
Architecture: all
Depends:
 curl,
 dmidecode,
 ethtool,
 ipmitool,
 iproute2,
 jq,
 libxml-xpath-perl,
 lldpd,
 lsb-base,
 lshw,
 lsscsi,
 netcat-traditional,
 nmap,
 pciutils,
 ${misc:Depends},
Breaks:
 openstack-cluster-installer (<< 27~),
 openstack-cluster-installer-agent (<< 27~),
 openstack-cluster-installer-utils (<< 27~),
Replaces:
 openstack-cluster-installer (<< 27~),
 openstack-cluster-installer-agent (<< 27~),
 openstack-cluster-installer-utils (<< 27~),
Description: automatic PXE and puppet-master installer for OpenStack - agent
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package contains the tools necessary both in the Debian Live environment
 and in the installed systems. This include the hardware discovery script, which
 conveniently, can be run manually after a system is installed, so that the
 hardware configuration of a node can be updated.

Package: openstack-cluster-installer-openstack-ci
Architecture: all
Depends:
 ipxe,
 jq,
 openssh-client,
 python3-openstackclient,
 q-text-as-data,
 wget,
 ${misc:Depends},
Description: automatic PXE and puppet-master installer for OpenStack - PoC
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package installs the scripting needed for running OCI on a CI using
 OpenStack itself (ie: OpenStack installed on VMs in a cloud).

Package: openstack-cluster-installer-poc
Architecture: all
Depends:
 bridge-utils,
 curl,
 haproxy,
 ipcalc,
 iptables,
 jq,
 lsb-base,
 net-tools,
 openipmi (>= 2.0.25),
 openstack-cluster-installer-cli (= ${binary:Version}),
 openstack-debian-images,
 openstack-pkg-tools,
 q-text-as-data,
 qemu-system,
 qemu-utils,
 ${misc:Depends},
Suggests:
 qemu-kvm,
Description: automatic PXE and puppet-master installer for OpenStack - PoC
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package installs a PoC (Proof of Concept) server running VMs to test,
 contribute and debug OCI. Note that you need at least 128 GB of RAM to run it
 (256 GB recommended), as it will provision 18 nodes, with 3 nodes for each
 role: controller, computes, volume and network, plus the OCI node.

Package: openstack-cluster-installer-utils
Architecture: all
Depends:
 csvkit,
 hdparm,
 iptables-persistent,
 openstack-cluster-installer-common (= ${binary:Version}),
 ssl-cert,
 ${misc:Depends},
Description: automatic PXE and puppet-master installer for OpenStack - utils
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package contains utilities that are going to be installed on the system
 of machines running in production. It contains programs like:
  * oci-report-status: reports once a machine has finished booting.
  * oci-first-boot: script that does the first puppet runs and setups puppet.
  * oci-build-cinder-volume-vg: create the LVM volume group for volume machines.
  * oci-build-nova-instances-vg: build a volume group for nova to use.
  * and many more like this...

Package: puppet-module-oci
Architecture: all
Depends:
 openstack-puppet-modules,
 puppet-module-aboe-chrony,
 puppet-module-camptocamp-postfix,
 puppet-module-cristifalcas-etcd,
 puppet-module-deric-zookeeper,
 puppet-module-etcddiscovery,
 puppet-module-magnum,
 puppet-module-pcfens-filebeat,
 puppet-module-placement,
 puppet-module-puppetlabs-mysql (>= 8.1.0),
 puppet-module-saz-ssh,
 puppet-module-tempest,
 puppet-module-voxpupuli-alternatives,
 puppet-module-voxpupuli-collectd,
 ${misc:Depends},
Description: automatic PXE and puppet-master installer for OpenStack - puppet module
 OCI (OpenStack Cluster Installer) is a software to provision an OpenStack
 clusters automatically. This package installs a provisioning machine, which
 uses the below components:
  * a DHCP server (isc-dhcp-server)
  * a PXE boot server (tftp-hpa)
  * a web server (apache2)
  * a puppet-master
 .
 Once computers in the cluster boot for the first time, a Debian live system is
 served by OCI over PXE, to act as a discovery image. This live system then
 reports the hardware features back to OCI. Computers can then be installed
 with Debian from that live system, configured with a puppet-agent that will
 connect to the puppet-master of OCI. After Debian is installed, the server
 reboots, and OpenStack services are provisionned, depending on the server role
 in the cluster.
 .
 OCI is fully packaged in Debian, including all of the Puppet modules. After
 installing the OCI package and its dependencies, no other artificat needs to
 be installed on your provisioning server, meaning that if a local debian
 mirror is available, the OpenStack cluster installation can be done completely
 offline.
 .
 This package contains the puppet module.
