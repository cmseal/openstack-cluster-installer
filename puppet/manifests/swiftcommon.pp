class oci::swiftcommon(
  $swift_store_account       = true,
  $swift_store_container     = true,
  $swift_store_object        = true,
){

  # setgid so created files inherit the group
  file { '/var/log/swift':
    ensure => directory,
    mode   => '2750',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-account-auditor.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-account.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-account-reaper.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-account-replicator.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-container-auditor.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-container.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-container-reconciler.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-container-replicator.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-container-sync.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-container-updater.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-proxy.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-object.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }->
  file { '/var/log/swift/swift-drive-audit.log':
    ensure => file,
    mode   => '0640',
    owner  => 'swift',
    group  => 'adm',
  }

  if $swift_store_account or $swift_store_container or $swift_store_object{
    package { 'swift-drive-audit':
      ensure => present,
    }

    swift_drive_audit_config {
      'drive-audit/log_file_pattern': value => '/var/log/kern-legacy.*[!.][!g][!z]';
    }
  }

  file { '/etc/rsyslog.d/10-kern-legacy.conf':
    ensure => present,
    source => 'puppet:///modules/oci/rsyslog/10-kern-legacy.conf',
    path => '/etc/rsyslog.d/10-kern-legacy.conf',
    group => 'root',
    owner => 'root',
    mode => '0644',
    require => [Package['rsyslog']],
    notify  => Service['rsyslog'],
  }

  logrotate::rule { 'kern-legacy':
    path           => '/var/log/kern-legacy.log',
    rotate         => '4',
    rotate_every   => 'week',
    missingok      => true,
    ifempty        => false,
    compress       => true,
    delaycompress  => true,
    postrotate     => '/usr/lib/rsyslog/rsyslog-rotate',
  }

  file { '/etc/rsyslog.d/swift.conf':
    ensure => absent,
  }

  file { '/etc/rsyslog.d/20-swift.conf':
    ensure  => present,
    source  => "puppet:///modules/oci/rsyslog/20-swift.conf",
    require => [Package['rsyslog'], File['/var/log/swift']],
    notify  => Service['rsyslog'],
  }

}
