class oci::debmirror(
  $machine_hostname         = undef,
  $machine_ip               = undef,
  $self_signed_api_cert     = true,

  # From variables.json
  $kernel_from_backports    = false,
){

  class { '::oci::puppet_oci_ca_cert':
    self_signed_api_cert => $self_signed_api_cert,
  }

  class {'::archvsync':
    manage_apache   => true,
    manage_pureftpd => true,
    package_ensure  => 'present',
    mirrorname      => $::fqdn,
    to              => '/home/ftp/debian/',
    mailto          => 'toto@example.com',
    homedir         => '/home/ftp',
    hub             => 'false',
    rsync_host      => 'ftp.fr.debian.org',
    rsync_path      => 'debian',
    info_maintainer => 'Toor Op <root@localhost>',
    info_sponsor    => 'World Company SA <https://www.example.com>',
    info_country    => 'US',
    info_location   => 'Nowhere city',
    info_throughput => '10Gb',
    arch_include    => 'amd64 source',
    arch_exclude    => '',
    logdir          => '/home/ftp/log',
  }
}
