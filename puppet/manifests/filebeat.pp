class oci::filebeat(
  $cluster_name       = undef,
  $machine_role       = undef,
  $filebeat_enable    = false,
  $filebeat_repo      = undef,

  # What to log
  $log_aodh           = false,
  $log_apache2        = false,
  $log_barbican       = false,
  $log_frr            = false,
  $log_ceilometer     = false,
  $log_ceph           = false,
  $log_cinder         = false,
  $log_cloudkitty     = false,
  $log_corosync       = false,
  $log_designate      = false,
  $log_etcd           = false,
  $log_haproxy        = false,
  $log_horizon        = false,
  $log_glance         = false,
  $log_gnocchi        = false,
  $log_heat           = false,
  $log_keystone       = false,
  $log_memcached      = false,
  $log_magnum         = false,
  $log_mysql          = false,
  $log_neutron        = false,
  $log_nova           = false,
  $log_octavia        = false,
  $log_openvswitch    = false,
  $log_panko          = false,
  $log_pacemaker      = false,
  $log_placement      = false,
  $log_rabbitmq       = false,
  $log_swiftproxy     = false,
  $log_swiftaccount   = false,
  $log_swiftcontainer = false,
  $log_swiftobject    = false,
  $log_zookeeper      = false,

  # Filebeat params
  $fields_under_root  = undef,
  $output_hosts       = undef,
  $logging_level      = undef,
  $logging_syslog     = true,
  $logging_files      = undef,
  $logging_files_path = undef,
  $logging_files_name = undef,
){

  if $filebeat_enable {

    $filebeat_repo_key = '-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.14 (GNU/Linux)

mQENBFI3HsoBCADXDtbNJnxbPqB1vDNtCsqhe49vFYsZN9IOZsZXgp7aHjh6CJBD
A+bGFOwyhbd7at35jQjWAw1O3cfYsKAmFy+Ar3LHCMkV3oZspJACTIgCrwnkic/9
CUliQe324qvObU2QRtP4Fl0zWcfb/S8UYzWXWIFuJqMvE9MaRY1bwUBvzoqavLGZ
j3SF1SPO+TB5QrHkrQHBsmX+Jda6d4Ylt8/t6CvMwgQNlrlzIO9WT+YN6zS+sqHd
1YK/aY5qhoLNhp9G/HxhcSVCkLq8SStj1ZZ1S9juBPoXV1ZWNbxFNGwOh/NYGldD
2kmBf3YgCqeLzHahsAEpvAm8TBa7Q9W21C8vABEBAAG0RUVsYXN0aWNzZWFyY2gg
KEVsYXN0aWNzZWFyY2ggU2lnbmluZyBLZXkpIDxkZXZfb3BzQGVsYXN0aWNzZWFy
Y2gub3JnPokBOAQTAQIAIgUCUjceygIbAwYLCQgHAwIGFQgCCQoLBBYCAwECHgEC
F4AACgkQ0n1mbNiOQrRzjAgAlTUQ1mgo3nK6BGXbj4XAJvuZDG0HILiUt+pPnz75
nsf0NWhqR4yGFlmpuctgCmTD+HzYtV9fp9qW/bwVuJCNtKXk3sdzYABY+Yl0Cez/
7C2GuGCOlbn0luCNT9BxJnh4mC9h/cKI3y5jvZ7wavwe41teqG14V+EoFSn3NPKm
TxcDTFrV7SmVPxCBcQze00cJhprKxkuZMPPVqpBS+JfDQtzUQD/LSFfhHj9eD+Xe
8d7sw+XvxB2aN4gnTlRzjL1nTRp0h2/IOGkqYfIG9rWmSLNlxhB2t+c0RsjdGM4/
eRlPWylFbVMc5pmDpItrkWSnzBfkmXL3vO2X3WvwmSFiQbkBDQRSNx7KAQgA5JUl
zcMW5/cuyZR8alSacKqhSbvoSqqbzHKcUQZmlzNMKGTABFG1yRx9r+wa/fvqP6OT
RzRDvVS/cycws8YX7Ddum7x8uI95b9ye1/Xy5noPEm8cD+hplnpU+PBQZJ5XJ2I+
1l9Nixx47wPGXeClLqcdn0ayd+v+Rwf3/XUJrvccG2YZUiQ4jWZkoxsA07xx7Bj+
Lt8/FKG7sHRFvePFU0ZS6JFx9GJqjSBbHRRkam+4emW3uWgVfZxuwcUCn1ayNgRt
KiFv9jQrg2TIWEvzYx9tywTCxc+FFMWAlbCzi+m4WD+QUWWfDQ009U/WM0ks0Kww
EwSk/UDuToxGnKU2dQARAQABiQEfBBgBAgAJBQJSNx7KAhsMAAoJENJ9ZmzYjkK0
c3MIAIE9hAR20mqJWLcsxLtrRs6uNF1VrpB+4n/55QU7oxA1iVBO6IFu4qgsF12J
TavnJ5MLaETlggXY+zDef9syTPXoQctpzcaNVDmedwo1SiL03uMoblOvWpMR/Y0j
6rm7IgrMWUDXDPvoPGjMl2q1iTeyHkMZEyUJ8SKsaHh4jV9wp9KmC8C+9CwMukL7
vM5w8cgvJoAwsp3Fn59AxWthN3XJYcnMfStkIuWgR7U2r+a210W6vnUxU4oN0PmM
cursYPyeV0NX/KQeUeNMwGTFB6QHS/anRaGQewijkrYYoTNtfllxIu9XYmiBERQ/
qPDlGRlOgVTd9xUfHFkzB52c70E=
=92oX
-----END PGP PUBLIC KEY BLOCK-----'

    apt::source {'filebeat':
      ensure   => 'present',
      location => $filebeat_repo,
      release  => 'stable',
      key      => {
        id      => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
        ensure  => 'present',
        content => $filebeat_repo_key,
      }
    }

    class { 'filebeat':
      manage_repo       => false,
      manage_apt        => false,
      major_version     => '7',
      fields_under_root => $fields_under_root,
      outputs           => {
          'logstash' => {
              'hosts'       => $output_hosts,
              'loadbalance' => true,
              'workers'     => 4,
            },
        },
      logging => {
          'level'     => $logging_level,
          'to_syslog' => $logging_syslog,
          'to_files'  => $logging_files,
          'files'     => {
              'path'        => $logging_files_path,
              'name'        => $logging_files_name,
              'keepfiles'   => '7',
              'permissions' => '0644',
            },
        },
      require => Apt::Source['filebeat'],
    }

    filebeat::input { 'system':
      fields_under_root => true,
      paths             => [ '/var/log/auth.log', '/var/log/daemon.log', '/var/log/mail.log', '/var/log/syslog', '/var/log/messages', ],
      fields            => { backend => $machine_role, },
      tags              => [ 'syslog', 'system', $cluster_name, ],
    }

    if $log_aodh{
      filebeat::input { 'aodh':
        fields_under_root => true,
        paths             => [ '/var/log/aodh/*.log*' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'aodh', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/aodh.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_apache2{
      filebeat::input { 'apache2-access':
        fields_under_root => true,
        paths             => [ '/var/log/apache2/*access.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'apache2', 'access', $cluster_name, ],
      }
      filebeat::input { 'apache2-error':
        fields_under_root => true,
        paths             => [ '/var/log/apache2/*error.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'apache2', 'error', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/apache2.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_barbican{
      filebeat::input { 'barbican-wsgi-api':
        fields_under_root => true,
        paths             => [ '/var/log/barbican/barbican-wsgi-api.log', '/var/log/barbican/barbican-manage.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'barbican', 'openstack', 'oslo', $cluster_name, ],
      }
      filebeat::input { 'barbican-api':
        fields_under_root => true,
        paths             => [ '/var/log/barbican/barbican-api.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'barbican', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/barbican.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_frr{
      filebeat::input { 'frr':
        fields_under_root => true,
        paths             => [ '/var/log/frr/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'frr', 'syslog', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/frr.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    filebeat::input { 'chrony':
      fields_under_root => true,
      paths             => [ '/var/log/chrony/*.log' ],
      fields            => { backend => $machine_role, },
      tags              => [ 'chrony', 'ntp', $cluster_name, ],
    }

    if $log_ceilometer{
      filebeat::input { 'ceilometer':
        fields_under_root => true,
        paths             => [ '/var/log/ceilometer/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'ceilometer', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/ceilometer.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_ceph{
      filebeat::input { 'ceph':
        fields_under_root => true,
        paths             => [ '/var/log/ceph/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'ceph', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/ceph.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_cinder{
      filebeat::input { 'cinder':
        fields_under_root => true,
        paths             => [ '/var/log/cinder/cinder-volume.log', '/var/log/cinder/cinder-backup.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'cinder', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/cinder.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_cloudkitty{
      filebeat::input { 'cloudkitty':
        fields_under_root => true,
        paths             => [ '/var/log/cloudkitty/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'cloudkitty', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/cloudkitty.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_corosync{
      filebeat::input { 'corosync':
        fields_under_root => true,
        paths             => [ '/var/log/corosync/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'corosync', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/corosync.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_designate{
      filebeat::input { 'designate':
        fields_under_root => true,
        paths             => [ '/var/log/designate/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'designate', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/designate.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_etcd{
      filebeat::input { 'etcd':
        fields_under_root => true,
        paths             => [ '/var/log/etcd/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'etcd', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/etcd.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_haproxy{
      filebeat::input { 'haproxy':
        fields_under_root => true,
        paths             => [ '/var/log/haproxy.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'haproxy', 'syslog', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/haproxy.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_horizon{
      filebeat::input { 'horizon':
        fields_under_root => true,
        paths             => ['/var/log/horizon/*.log', '/var/log/openstack-dashboard/*.log'],
        fields            => { backend => $machine_role, },
        tags              => [ 'horizon', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/horizon.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_glance{
      filebeat::input { 'glance-api':
        fields_under_root => true,
        paths             => [ '/var/log/glance/api.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'glance', 'openstack', 'oslo', $cluster_name, ],
      }
      filebeat::input { 'glance-api-uwsgi':
        fields_under_root => true,
        paths             => [ '/var/log/glance/glance-api.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'glance', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/glance.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_gnocchi{
      filebeat::input { 'gnocchi':
        fields_under_root => true,
        paths             => [ '/var/log/gnocchi/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'gnocchi', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/gnocchi.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_heat{
      filebeat::input { 'heat':
        fields_under_root => true,
        paths             => [ '/var/log/heat/heat-api-cfn.log', '/var/log/heat/heat-engine.log', '/var/log/heat/heat-purge_deleted.log', '/var/log/heat/heat-wsgi-api-cfn.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'heat', 'openstack', 'oslo', $cluster_name, ],
      }
      filebeat::input { 'heat-api':
        fields_under_root => true,
        paths             => [ '/var/log/heat/heat-api.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'heat', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/heat.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_keystone{
      filebeat::input { 'keystone':
        fields_under_root => true,
        paths             => [ '/var/log/keystone/keystone-admin.log', '/var/log/keystone/keystone-public.log', '/var/log/keystone/keystone-wsgi-public.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'keystone', 'openstack', 'oslo', $cluster_name, ],
      }
      filebeat::input { 'keystone-api':
        fields_under_root => true,
        paths             => [ '/var/log/keystone/keystone.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'keystone', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/keystone.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_memcached{
      filebeat::input { 'memcached':
        fields_under_root => true,
        paths             => [ '/var/log/memcached/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'memcached', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/memcached.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_magnum{
      filebeat::input { 'magnum':
        fields_under_root => true,
        paths             => [ '/var/log/magnum/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'magnum', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/magnum.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_mysql{
      filebeat::input { 'mysql':
        fields_under_root => true,
        paths             => [ '/var/log/mysql/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'mysql', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/mysql.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_neutron{
      filebeat::input { 'neutron':
        fields_under_root => true,
        paths             => [ '/var/log/neutron/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'neutron', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/neutron.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_nova{
      filebeat::input { 'nova':
        fields_under_root => true,
        paths             => [ '/var/log/nova/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'nova', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/nova.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_octavia{
      filebeat::input { 'octavia':
        fields_under_root => true,
        paths             => [ '/var/log/octavia/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'octavia', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/octavia.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_openvswitch{
      filebeat::input { 'openvswitch':
        fields_under_root => true,
        paths             => [ '/var/log/openvswitch/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'openvswitch', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/openvswitch.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_panko{
      filebeat::input { 'panko':
        fields_under_root => true,
        paths             => [ '/var/log/panko/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'panko', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/panko.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_pacemaker{
      filebeat::input { 'pacemaker':
        fields_under_root => true,
        paths             => [ '/var/log/pacemaker/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'pacemaker', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/pacemaker.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_placement{
      filebeat::input { 'placement':
        fields_under_root => true,
        paths             => [ '/var/log/placement/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'placement', 'openstack', 'oslo', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/placement.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_rabbitmq{
      filebeat::input { 'rabbitmq':
        fields_under_root => true,
        paths             => [ '/var/log/rabbitmq/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'rabbitmq', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/rabbitmq.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_swiftproxy{
      filebeat::input { 'swiftproxy':
        fields_under_root => true,
        paths             => [ '/var/log/swift/proxy.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'proxy', 'openstack', 'syslog', $cluster_name, ],
      }
      filebeat::input { 'swiftproxy-uwsgi':
        fields_under_root => true,
        paths             => [ '/var/log/swift/swift-proxy.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'proxy', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/swiftproxy.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_swiftaccount{
      filebeat::input { 'swiftaccount':
        fields_under_root => true,
        paths             => [ '/var/log/swift/account.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'account', 'openstack', 'syslog', $cluster_name, ],
      }
      filebeat::input { 'swiftaccount-uwsgi':
        fields_under_root => true,
        paths             => [ '/var/log/swift/swift-account.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'account', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/swiftaccount.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_swiftcontainer{
      filebeat::input { 'swiftcontainer':
        fields_under_root => true,
        paths             => [ '/var/log/swift/container.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'container', 'openstack', 'syslog', $cluster_name, ],
      }
      filebeat::input { 'swiftcontainer-uwsgi':
        fields_under_root => true,
        paths             => [ '/var/log/swift/swift-container.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'container', 'openstack', 'uwsgi', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/swiftcontainer.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_swiftobject{
      filebeat::input { 'swiftobject':
        fields_under_root => true,
        paths             => [ '/var/log/swift/object.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'swift', 'object', 'openstack', 'syslog', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/swiftobject.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

    if $log_zookeeper{
      filebeat::input { 'zookeeper':
        fields_under_root => true,
        paths             => [ '/var/log/zookeeper/*.log' ],
        fields            => { backend => $machine_role, },
        tags              => [ 'zookeeper', $cluster_name, ],
      }
    }else{
      file { '/etc/filebeat/conf.d/zookeeper.yml': ensure => 'absent', notify => Service['filebeat'] }
    }

  }
}
