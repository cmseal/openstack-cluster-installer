class oci::dns(
  $region_name              = 'RegionOne',
  $openstack_release        = undef,		# rocky, stein, train
  $cluster_name             = undef,		# z
  $machine_hostname         = undef,		# z-controller-1.example.com
  $machine_ip               = undef,		# 192.168.101.2
  $vip_hostname             = undef,
  $all_masters              = undef,
  $all_masters_ip           = undef,

  $pass_designate_rndckey   = undef, # hmac-sha256 rndc key

  # These are from variables.json
  $kernel_from_backports    = undef,
){
  ensure_resource('group', 'zabbix', {
    'ensure' => 'present',
    'gid'    => '966',
  })

  ##########################################################################
  ### Add a few package so that installing the Octavia service is easier ###
  ##########################################################################
  package { 'openstack-pkg-tools':
    ensure => present,
  }

  $all_masters_dot_comma = join($all_masters_ip,';')
  $all_masters_ip_allow = "${all_masters_dot_comma}"

  $controls = {
    '0.0.0.0' => {
      'port'              => 953,
      'allowed_addresses' => [ $all_masters_ip_allow ],
      'keys'              => [ 'designate-rndc-key' ]
    },
  }

  class { '::dns':
    namedconf_template => 'oci/named.conf.erb',
    recursion          => 'no',
    allow_recursion    => [],
    empty_zones_enable => 'yes',
    dns_notify         => 'yes',
    controls           => $controls,
    additional_options => {
        'listen-on'       => 'port 53 { any; }',
        'listen-on-v6'    => 'port 53 { any; }',
        'allow-new-zones' => 'yes',
      },
  }
  file {'/etc/bind/designate-rndc.key':
    ensure                  => 'present',
    owner                   => 'root',
    group                   => 'bind',
    mode                    => '0640',
    selinux_ignore_defaults => true,
    content                 => "key \"designate-rndc-key\" {
        algorithm hmac-sha256;
        secret \"${pass_designate_rndckey}\";
};
",
    notify                  => Service['bind9'],
  }

}
